package fr.wynalia.generics;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        getLogger().info("Loading..");
    }

    @Override
    public void onDisable() {
        getLogger().info("Unloading..");
    }
}
