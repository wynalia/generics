package fr.wynalia.generics.teams;

import net.md_5.bungee.api.ChatColor;

import java.util.List;
import java.util.UUID;

public class GTeam {
    private final int id;
    private final String name;
    private final ChatColor color;
    private final List<UUID> members;

    public GTeam(int id, String name, ChatColor color, List<UUID> members) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.members = members;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }

    public List<UUID> getMembers() {
        return members;
    }
}
