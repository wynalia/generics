package fr.wynalia.generics.teams;

import fr.wynalia.generics.templates.Template;
import fr.wynalia.generics.templates.TemplateReader;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

public class GTeamListener implements Listener {
    private final Plugin plugin;

    public GTeamListener(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        Template template = TemplateReader.read(plugin, "chat.player-join");
        template.set("username", player.getName());
        event.setJoinMessage(template.render());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Template template = TemplateReader.read(plugin, "chat.player-quit");
        template.set("username", player.getName());
        event.setQuitMessage(template.render());
    }

    @EventHandler
    public void onTalk(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        GTeam team = GTeamManagement.getTeam(player.getUniqueId());
        Template template;

        if (event.getMessage().startsWith("!")) {
            event.setMessage(event.getMessage().substring(1));
            template = TemplateReader.read(plugin, "chat.player-talk-global");
            template.set("team", team.getColor() + team.getName());
            template.set("username", player.getName());
            template.set("message", event.getMessage());
            event.setFormat(template.render());
        } else {
            event.setCancelled(true);
            template = TemplateReader.read(plugin, "chat.player-talk-team");
            template.set("username", team.getColor() + player.getName());
            template.set("message", event.getMessage());
            GTeamManagement.sendMessage(team, template);
        }
    }
}
