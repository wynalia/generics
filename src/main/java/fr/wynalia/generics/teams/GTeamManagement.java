package fr.wynalia.generics.teams;

import fr.wynalia.generics.databases.DatabaseManager;
import fr.wynalia.generics.templates.Template;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GTeamManagement {
    private static final Map<Integer, GTeam> teams = new HashMap<>();

    public GTeamManagement(Plugin plugin) {
        String event = plugin.getConfig().getString("db.event");
        String sql = "SELECT * FROM " + event + "_teams t " +
                "LEFT JOIN " + event + "_players p " +
                "ON p.team_id = t.id";

        try (PreparedStatement statement = DatabaseManager.getConnection().prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("t.id");
                String name = resultSet.getString("t.name");
                ChatColor color = ChatColor.of(resultSet.getString("t.color"));
                UUID member = UUID.fromString(resultSet.getString("p.uuid"));

                GTeam team = teams.getOrDefault(id, new GTeam(id, name, color, new ArrayList<>()));
                team.getMembers().add(member);

                teams.put(id, team);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static GTeam getTeam(UUID uuid) {
        return teams.values().stream()
                .filter(team -> team.getMembers().contains(uuid))
                .findAny()
                .orElse(null);
    }

    public static void sendMessage(GTeam team, Template template) {
        for (UUID uuid : team.getMembers()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) player.sendMessage(template.render());
        }
    }
}
