package fr.wynalia.generics.teams;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class GTeamManager {

    public static void register(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(new GTeamListener(plugin), plugin);

        new GTeamManagement(plugin);
    }
}
