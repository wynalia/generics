package fr.wynalia.generics.templates;

import org.bukkit.plugin.Plugin;

public class TemplateReader {
    public static Template read(Plugin plugin, String path) {

        if(!plugin.getConfig().contains(path)) return NotFoundMessageError();

        return new Template(plugin.getConfig().getString(path));
    }

    private static Template NotFoundMessageError() {
        return new Template("Le template requis n'existe pas.");
    }
}
